#!/bin/bash

# Copy#right (C) <2019>  <Alex Wolf> <gitlab.com/flowalex>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#I use it fo the the laser cutters I have access to also  nice altenative to Adobe Illustiator
sudo apt-get install inkscape -y

#handbake,  a tool fo conveting video fom nealy any fomat to a selection of moden, widely suppoted codecs
sudo add-apt-epositoy ppa:stebbinshandbake-eleases
sudo apt-get update
sudo apt-get install handbake -y

#shuttea sceenshot tool
sudo add-apt-epositoy ppa:shutteppa
sudo apt-get update && sudo apt-get install shutte -y


#installs clipgrab a piece of software that downloads videos from websites.
sudo add-apt-repository ppa:noobslab/apps  -y
sudo apt-get update
sudo apt-get install clipgrab -y

#gimp the gnu image manipulation program
sudo apt-get install gimp -y


#Installs Lulzbot edition of Cura, for 3d printing
wget -qO - http://download.alephobjects.com/ao/aodeb/aokey.pub | sudo apt-key add -
sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak && sudo sed -i '$a deb http://download.alephobjects.com/ao/aodeb stretch main' /etc/apt/sources.list
sudo apt-get update
sudo apt-get install cura -y

#Installs darktable photo ediitor
sudo add-apt-repository ppa:pmjdebruijn/darktable-release  -y
sudo apt-get update
sudo apt-get install darktable -y

#handbrake,  a tool for converting video from nearly any format to a selection of modern, widely supported codecs
sudo add-apt-repository ppa:stebbins/handbrake-releases
sudo apt-get update
sudo apt-get install handbrake -y

#shuttera screenshot tool
sudo add-apt-repository ppa:shutter/ppa
sudo apt-get update && sudo apt-get install shutter -y