#!/bin/bash

sudo add-apt-repository ppa:noobslab/macbuntu -y
sudo apt-get update

#Optional since I don't use Wayland I use Plank dock but there is a good dash to dock in gnome shell extensions (if using gnome shell)
sudo apt-get install plank -y