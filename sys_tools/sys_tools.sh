#!/bin/bash

# Copyright (C) <2019>  <Alex Wolf> <gitlab.com/flowalex>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

 wget http://mirrors.kernel.org/ubuntu/pool/main/libp/libpng/libpng12-0_1.2.54-1ubuntu1_amd64.deb
sudo gdebi --non-interactive libpng12-0_1.2.54-1ubuntu1_amd64.deb




#Adding gdebi for the apps that aren't in package managers so  that the apps that are downloaded from the web can be installed
sudo apt-get install gdebi -y

#Just a good program to  have installed
sudo apt-get install gnome-tweak-tool -y

# stacer a system monitoring tool
sudo add-apt-repository ppa:oguzhaninan/stacer
sudo apt-get update
sudo apt-get install stacer

#syncthing app that syncronizes files across mutiple computers, still needs to be configured after installation for the folders that you want to  sync and to which devices they want to sync them to.
curl -s https://syncthing.net/release-key.txt | sudo apt-key add -
# Add the "stable" channel to your APT sources:
echo "deb https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list
# Update and install syncthing:
sudo apt-get update
sudo apt-get install syncthing -y

#fun terminal app that gives information about the operating system that you are using and other details about the system.
sudo apt install neofetch -y