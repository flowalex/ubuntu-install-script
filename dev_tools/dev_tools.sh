#!/bin/bash

# Copyright (C) <2019>  <Alex Wolf> <gitlab.com/flowalex>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Gitkraken a GUI git client
wget https://release.gitkraken.com/linux/gitkraken-amd64.deb
dpkg -i gitkraken-amd64.deb

# vscodium vs code without the extra telementary from Microsoft
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | sudo apt-key add -
echo 'deb https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list
sudo apt update
sudo apt install vscodium

# Zshell instead of bash
sudo apt install zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

#installs gnome-builder, I think that it is a great IDE for linux, OS, POP!_OS is based off of Ubuntu with the gnome desktop enviroment.
sudo apt-get install gnome-builder -y

#QEMU a generic and open source machine emulator and virtualizer.
sudo apt-get install qemu -y