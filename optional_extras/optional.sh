#!/bin/bash


#I choose keepassx since I prefer it to keepass2 but both are good to use.
sudo apt-get install keepassx -y
#A tool that lets you use the auto type for keepass
sudo apt-get install xdotool -y

#some fun terminal apps
#steam locomotive
sudo apt-get install sl -y

#cowsay
sudo apt-get install cowsay -y

#lolcat
sudo apt-get install lolcat

sudo apt-get install figlet

wget -O mac-fonts.zip http://drive.noobslab.com/data/Mac/macfonts.zip
sudo unzip mac-fonts.zip -d /usr/share/fonts; rm mac-fonts.zip
sudo fc-cache -f -v

#installs the application cool retro term, while not necessary,  it is a fun terminal emulator to have, quoting Bryan Lunduke "Who doesn't want thei terminal to look like an old IBM computer
sudo add-apt-repository ppa:noobslab/apps -y
# echo "deb http://archive.ubuntu.com/ubuntu/ xenial main universe" | sudo tee /etc/apt/sources.list.d/depd-CRT.list
sudo apt-get update
sudo apt-get install cool-retro-term -y
sudo apt-get install qml-module-qt-labs-settings qml-module-qt-labs-folderlistmodel -y
sudo apt-get -f install;sudo apt-get install cool-retro-term -y
sudo dpkg -i noninteractive  --force-overwrite /var/cache/apt/archives/cool-retro-term*.deb;sudo rm /etc/apt/sources.list.d/depd-CRT.list;
sudo apt-get update