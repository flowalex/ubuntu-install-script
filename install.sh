#!bin/bash

echo "Copyright (C) <2019>  <Alex Wolf> <gitlab.com/flowalex>
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    The script will install programs that are useful for my daily workflow and
    other customizations that are for my prefrence, anyone can add or remove
    apps from the script, if there are any other important apps missing for
    your daily needs."


echo "Do you accept the liscense terms [y/n]?"
read ans


if [ $ans = y -o $ans = Y -o $ans = yes -o $ans = Yes -o $ans = YES ]
  then
    mkdir installationfiles

    while `test $ans='y'`
      do
        echo "Menu"
        echo "1) Update"
        echo "2) System tools"
        echo "3) Creative tools"
        echo "4) Media"
        echo "5) Dev tools"
        echo "6)"
        echo "7)"
        echo "8)"
        echo "9) Elementary apps"
	      echo "10) Exit"
        #echo ""
        read choice
        case $choice in

          1) #Runs an initial update to  make sure that the OS is ready for the apps that are in the Ubuntu Repositories
          sudo apt-get update && sudo apt-get upgrade
	        ;;
          2) # Runs tools related to system tasks
	        echo "System Tools"
	        sudo bash $PWD/sys_tools/sys_tools.sh
	        ;;
	        10) exit
          ;;
          esac
  done
fi
